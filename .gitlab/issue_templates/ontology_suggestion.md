# Issue Title (add to "Title" field ):
Should follow this pattern: Ontology suggestion: [ontology name] - [your context (project, institution, consortium, etc.)]

e.g. `Ontology suggestion: Qudt - Metadata4Ing`

# Fill Ontology Metadata in YAML
You should provide some information on the ontology, for example, what is its title, what is it about, who created it, under what license has it been published?

This information helps us to decide whether the ontology can be added to the NFDI4Ing Terminology Service.

It is also important for NFDI4Ing Terminology Service users to understand what the scope of the ontology is. 

Some information about the ontology is obligatory (ontology_purl, title, id, preferredPrefix, license).

Please use the following schema to provide the information. If unavailable or unknown, just leave the respective line empty.

```yaml
ontology_purl: replace this text by the ontology's URI 
title: replace this text by the ontology's title
preferredPrefix: add the preferred prefix used by the ontology
id: repeat the ontologys's preferred prefix
license:
  label: provide the name of the ontology's license
  url: name the URL of the licence deed of the ontology
description: add the description of the ontology
homepage: provide a URL to a homepage related to the ontology if available, e.g. a documentation
mailing_list: provide the adress of a mailing list dedicated to the ontology
tracker: if the ontolgy is developed on an online platform like GitHub or GitLab provide the link to the ontology's issue tracker
creator:
  - provide a list of the ontology creators if known
```
