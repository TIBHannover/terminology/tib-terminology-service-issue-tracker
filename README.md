# Terminology Service Issue Tracker

This is the old issue tracker of the [TIB Terminology Service](https://service.tib.eu/ts4tib/index).

If you would like to send us a message, please use our [contact form](https://terminology.tib.eu/ts/contact).

If you would like to suggest a new ontology, please use our [ontology suggestion form](https://terminology.tib.eu/ts/ontologysuggestion).

If you would like to report a technical issue, especially regarding the API, please use [our ols4 repo at GitHub](https://github.com/TIBHannover/ols4).